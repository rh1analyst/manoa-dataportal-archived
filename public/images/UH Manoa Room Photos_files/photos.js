//PAGE PREP
//resize content-wrapper
resize();

//hide content, apply spinner
d3.select('#content-wrapper').style('display','none');

//PAGE AUTH
password = sessionStorage.getItem("key0");
username = sessionStorage.getItem("key1");

// Add an additional else if clause to the if statement and add new user's credentials below 
if (password == 'mkoffice' && username == 'user'    ||
    password == 'manoa' && username == 'jkurata'    ||
    password == 'manoa' && username == 'jgouveia'   ||
    password == 'rainbowwarriors' && username == 'Guest'
    ) {
    console.log('login:success');
} else {
    logout();
}

function logout() {
    window.open('index.html', '_self')
}

//GOOGLE AUTH
var CLIENT_ID = '242641968476-jmeo6n7kkt71rmlg55e0hb4c2d569gg6.apps.googleusercontent.com';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];
var SCOPES = 'https://www.googleapis.com/auth/drive';

var authorizeModal = document.getElementById('authorize-modal');
var authorizeButton = document.getElementById('authorize-button');

function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

function initClient() {
  gapi.client.init({
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
  });
}

function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    authorizeModal.style.display = 'none';
    authorizeButton.style.display = 'none';
    initContent();
    console.log('signed in..')
  } else {
    authorizeModal.style.display = 'block';
    authorizeButton.style.display = 'block';
  }
}

function handleAuthClick(event) {
  gapi.auth2.getAuthInstance().signIn();
}


//VAR DEFINITION

//active selections
active_bldg = '';
active_room = '';
bldgfiles = [];
roomfiles = [];

//INIT SEQUENCE

function initContent() {
	var target = document.getElementById('spin-area');
	spinner = new Spinner({lines: 9, length: 9, width: 5, radius: 14, color: "#525252", speed: 1.9, trail: 40, className: "spinner"}).spin(target);
	//load data
	loadData();
}

function loadData() {
	d3.queue()
		.defer(d3.csv, "data/bldgs.csv")
		.await(loadBldgs);

	d3.queue()
		.defer(d3.csv, "data/rooms.csv")
		.await(loadRooms);

	function loadBldgs(error, _bldgdata) {
		if(error) {
			console.log(error);
		}

		bldgdata = _bldgdata;
		console.log(bldgdata);
		initBldgSelectionList();
	}

 	function loadRooms(error, _roomdata) {
		if(error) {
			console.log(error);
		}

		roomdata = _roomdata;
		console.log(roomdata);
	}

	setTimeout(function () {
		spinner.stop();
		d3.select('#content-wrapper').style('display','block');
	},1000);
}

function initBldgSelectionList() {

	//api call for folder names + ids
	gapi.client.drive.files.list({
		'orderBy': "folder, name",
		'q': "(\'0ByZIs606S7oWcVdxN0M4WWo0alE\' in parents)",
		'fields': "nextPageToken, files(id, name)",
		'pageSize': 500,
	}).then(function(response){
		var files = response.result.files;
		
		//loop response
		for (var i = 0; i < files.length; i++) {
			//construct array of folder names + ids
			bldgfiles.push({'bldg':files[i].name,'fileid':files[i].id});
		}

		//helper function for select2, formatting list text with img
		function formatBldg(bldg) {
			if (bldgfiles.filter(function(d) { return d.bldg == bldg.text }).length == 0) {
				return $('<span style="margin:0px 0px 0px 16px;">' + bldg.text + '</span>')
			} else {
				var $bldg = $('<span><i class="material-icons" style="font-size:13px; margin:0px 3px 0px 0px; color:#1f4859;">photo_camera</i><span style="position:relative; top:-2px;">' + bldg.text + '</span></span');
				return $bldg
			}
		}

		//helper function for select2, sorting list options alphabetically
		function sortBldgs(data) {
			return data.sort(function (a, b) {
				if (a.text > b.text) {
					return 1;
				} else if (a.text < b.text) {
					return -1;
				} else {
					return 0;
				}
			});
		}

		//init bldg menu
		var bldgmenu = d3.select("#bldg-menu select");

		//populate selection list
		bldgmenu.selectAll('option')
			.data(d3.map(bldgdata, function (d) { return d.bldg }).keys())
			.enter().append('option')
			.attr('value',function(d) { return d })
			.text(function(d) { return d });

		//insert empty option tag for default selection
		bldgmenu._groups[0][0].innerHTML = '<option></option>' + bldgmenu._groups[0][0].innerHTML;

		//initialize selection list
		$('#bldg-menu select').select2({
			placeholder: "Select Building",
			dropdownAutoWidth : true,
			width: '400px',
			formatResult: formatBldg, //add camera img if photos are available
			formatSelection: formatBldg, //add camera img if photos are available
			sorter: sortBldgs,
		}).on('change', selectBuilding);


		function selectBuilding() {

			//update active bldg variable
			if (bldgfiles.filter(function(d) {return d.bldg == bldgmenu.property('value') }).length > 0) {
				active_bldg = bldgfiles.filter(function(d) {return d.bldg == bldgmenu.property('value') })[0];
			} else {
				active_bldg = {'bldg':bldgmenu.property('value'),'fileid':'none'}
			}
			
			//reveal clear button
			d3.select('#bldg-menu .clear').style('display','inline-block');

			//init roomlist for selected bldg
			initRoomSelectionList();
		}
	});
}

function initRoomSelectionList() {

	//parse full room list down to rooms associated with selected building
	var _roomdata = roomdata.filter(function(d) { return d.bldg == active_bldg.bldg });

	if (active_bldg.fileid == 'none') {
		console.log('no photos!!');
	} else if (roomfiles.filter(function(d) { return d.bldg == active_bldg.bldg }).length > 0) {
		console.log('files already loaded!!');
	} else {
		//api call for folder names + ids
		gapi.client.drive.files.list({
			'orderBy': "folder, name",
			'q': "(\'" + active_bldg.fileid + "\' in parents)",
			'fields': "nextPageToken, files(id, name)",
			'pageSize': 500,
		}).then(function(response) {
			var files = response.result.files;

			//loop response
			for (var i = 0; i < files.length; i++) {
				//construct array of folder names + ids
				roomfiles.push({'bldg':active_bldg.bldg,'room':files[i].name,'fileid':files[i].id});
			}

			//helper function for select2, formatting list text with img
			function formatRoom(room) {
				if (roomfiles.filter(function(d) { return d.bldg == active_bldg.bldg && d.room == room.text }).length == 0) {
					return $('<span style="margin:0px 0px 0px 16px;">' + room.text + '</span>')
				} else {
					var $room = $('<span><i class="material-icons" style="font-size:13px; margin:0px 3px 0px 0px; color:#1f4859;">photo_camera</i><span style="position:relative; top:-2px;">' + room.text + '</span></span');
					return $room
				}
			}

			//helper function for select2, sorting list options alphabetically
			function sortRooms(data) {
				return data.sort(function (a, b) {
					if (a.text > b.text) {
						return 1;
					} else if (a.text < b.text) {
						return -1;
					} else {
						return 0;
					}
				});
			}

			//init room menu
			var roommenu = d3.select("#room-menu select");

			//clear selection list options
			d3.selectAll('#room-menu select').html('');

			//populate selection list
			roommenu.selectAll('option')
				.data(d3.map(_roomdata, function (d) {return d.room }).keys())
				.enter().append('option')
				.text(function(d) { return d.replace('\'','') });

			//insert empty option tag for default selection
			bldgmenu._groups[0][0].innerHTML = '<option></option>' + bldgmenu._groups[0][0].innerHTML;

			//reveal room menu
			d3.select('#room-menu').style('display','inline-block');
	
			//initialize selection list options
			$('#room-menu select').select2({
				placeholder: "Select Room",
				dropdownAutoWidth : true,
				width: '400px',
				formatResult: formatRoom, //add camera img if photos are available
				formatSelection: formatRoom, //add camera img if photos are available
				sorter: sortRooms,
			}).on('change', console.log('hi'));


		});
	}
}









function resize() {
    //resize wrapper
    var winHeight = $(window).height();
    $('#content-wrapper').height(winHeight - 115);
}

