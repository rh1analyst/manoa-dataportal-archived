//PAGE PREP
//resize content-wrapper
resize();

//hide content, apply spinner
d3.select('#content-wrapper').style('display','none');

//PAGE AUTH
password = sessionStorage.getItem("key0");
username = sessionStorage.getItem("key1");

// Add an additional else if clause to the if statement and add new user's credentials below 
if (username.toLowerCase() == 'user' && password.toLowerCase() == 'mkoffice' 	|| 
		username.toLowerCase() == 'mau' && password.toLowerCase() == 'rainbowwarriors' 	|| 
		username.toLowerCase() == 'jgouveia' && password.toLowerCase() == 'manoa' 	|| 
		username.toLowerCase() == 'rodwell' && password.toLowerCase() == 'rainbowwarriors' ||
		username.toLowerCase() == 'uranakak@hawaii.edu' && password.toLowerCase() == 'rainbowwarriors' ||
		username == 'Guest2019' && password == 'rainbowwarriors') {
	console.log('login: success!');
} else {
    logout();
}

function logout() {
    window.open('index.html', '_self')
}

//GOOGLE AUTH
var CLIENT_ID = '242641968476-jmeo6n7kkt71rmlg55e0hb4c2d569gg6.apps.googleusercontent.com';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];
var SCOPES = 'https://www.googleapis.com/auth/drive';

var authorizeModal = document.getElementById('authorize-modal');
var authorizeButton = document.getElementById('authorize-button');

function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

function initClient() {
  gapi.client.init({
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
  });
}

function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    authorizeModal.style.display = 'none';
    authorizeButton.style.display = 'none';
    initContent();
  } else {
    authorizeModal.style.display = 'block';
    authorizeButton.style.display = 'block';
  }
}

function handleAuthClick(event) {
  gapi.auth2.getAuthInstance().signIn();
}


//VAR DEFINITION

//active selections
active_bldg = '';
active_room = '';
bldgfiles = [];
roomfiles = [];

//INIT SEQUENCE

function initContent() {
	var target = document.getElementById('spin-area');
	spinner = new Spinner({lines: 9, length: 9, width: 5, radius: 14, color: "#525252", speed: 1.9, trail: 40, className: "spinner"}).spin(target);
	//load data
	loadData();
}

function loadData() {
	d3.queue()
		.defer(d3.csv, "data/bldgs.csv")
		.await(loadBldgs);

	d3.queue()
		.defer(d3.csv, "data/rooms.csv")
		.await(loadRooms);

	function loadBldgs(error, _bldgdata) {
		if(error) {
			console.log(error);
		}

		bldgdata = _bldgdata;
		initBldgSelectionList();
	}

 	function loadRooms(error, _roomdata) {
		if(error) {
			console.log(error);
		}

		roomdata = _roomdata;
	}

	setTimeout(function () {
		spinner.stop();
		d3.select('#content-wrapper').style('display','block');
	},1000);
}

function initBldgSelectionList() {

	//api call for folder names + ids
	gapi.client.drive.files.list({
		'orderBy': "folder, name",
		'q': "(\'0ByZIs606S7oWcVdxN0M4WWo0alE\' in parents)",
		'fields': "nextPageToken, files(id, name)",
		'pageSize': 500,
	}).then(function(response){
		var files = response.result.files;
		
		//loop response
		for (var i = 0; i < files.length; i++) {
			//construct array of folder names + ids
			bldgfiles.push({'bldg':files[i].name,'fileid':files[i].id});
		}

		listBldgs();
	});
}

function listBldgs () {

	//helper function for select2, formatting list text with img
	function formatBldg(bldg) {
		if (bldgfiles.filter(function(d) { return d.bldg == bldg.text }).length == 0) {
			return $('<span style="margin:0px 0px 0px 16px;">' + bldg.text + '</span>')
		} else {
			var $bldg = $('<span><i class="material-icons" style="font-size:13px; margin:0px 3px 0px 0px; color:#1f4859;">photo_camera</i><span style="position:relative; top:-2px;">' + bldg.text + '</span></span');
			return $bldg
		}
	}

	// //sort list options
	// var reA = /[^a-zA-Z]/g;
	// var reN = /[^0-9]/g;

	// bldgdata.sort(function (a, b) {
	// 	var aA = a.bldg.replace(reA, "");
	// 	var bA = b.bldg.replace(reA, "");
	// 	if(aA === bA) {
	// 		return 1;
	// 	} else if (a.bldg.text < b.bldg.text) {
	// 		var aN = parseInt(a.bldg.replace(reN, ""), 10);
	// 		var bN = parseInt(b.bldg.replace(reN, ""), 10);
	// 		return aN === bN ? 0 : aN > bN ? 1 : -1;
	// 	} else {
	// 		return aA > bA ? 1 : -1;
	// 	}
	// });

	//destroy existing select2
	$('#bldg-menu select').select2('destroy');
	$('#bldg-menu').html('<select><select><i style="display:none;" class="clear material-icons">close</i>');

	//init room menu
	var bldgmenu = d3.select("#bldg-menu select");

	//populate selection list
	bldgmenu.selectAll('option')
		.data(d3.map(bldgdata, function (d) { return d.bldg }).keys())
		.enter().append('option')
		.attr('value',function(d) { return d })
		.text(function(d) { return d });

	//insert empty option tag for default selection
	bldgmenu._groups[0][0].innerHTML = '<option></option>' + bldgmenu._groups[0][0].innerHTML;
	
	//initialize selection list
	$('#bldg-menu select').select2({
		placeholder: "Select Building",
		dropdownAutoWidth : true,
		width: '400px',
		formatResult: formatBldg, //add camera img if photos are available
	}).on('change', selectBuilding);

	function selectBuilding() {

		//update active bldg variable
		if (bldgfiles.filter(function(d) {return d.bldg == bldgmenu.property('value') }).length > 0) {
			active_bldg = bldgfiles.filter(function(d) {return d.bldg == bldgmenu.property('value') })[0];
		} else {
			active_bldg = {'bldg':bldgmenu.property('value'),'fileid':'none'}
		}
		
		//reveal,configure clear button
		d3.select('#bldg-menu .clear')
			.style('display','inline-block')
			.on('click',function() {
				
				//reset bldg selection list
				listBldgs();

				//reset active bldg, room
				active_bldg = '';
				active_room = '';

				//clear existing photo holder elements
				d3.select('#photo-frame').html('');
				d3.select('#thumbnail-liner').html('');

				//clear + hide data table
				d3.select('#room-info').style('display','none').html('');

				//hide clear buttons
				d3.select('#bldg-menu .clear').style('display','none');
				d3.select('#room-menu .clear').style('display','none');

				//hide room-selector element
				d3.select('#room-menu').style('display','none');

			});

		//init roomlist for selected bldg
		initRoomSelectionList();
	}
}

function initRoomSelectionList() {

	if (active_bldg.fileid == 'none' || roomfiles.filter(function(d) { return d.bldg == active_bldg.bldg }).length > 0) {
		listRooms();

	} else {
		//api call for folder names + ids
		gapi.client.drive.files.list({
			'orderBy': "folder, name",
			'q': "(\'" + active_bldg.fileid + "\' in parents)",
			'fields': "nextPageToken, files(id, name)",
			'pageSize': 500,
		}).then(function(response) {
			var files = response.result.files;

			//loop response
			for (var i = 0; i < files.length; i++) {
				//construct array of folder names + ids
				roomfiles.push({'bldg':active_bldg.bldg,'room':files[i].name,'fileid':files[i].id});
			}

			listRooms();
		});
	}
}

function listRooms() {

	//parse full room list down to rooms associated with selected building
	var _roomdata = roomdata.filter(function(d) { return d.bldg == active_bldg.bldg });

	//helper function for select2, formatting list text with img
	function formatRoom(room) {
		if (roomfiles.filter(function(d) { return d.bldg == active_bldg.bldg && d.room == room.text }).length == 0) {
			return $('<span style="margin:0px 0px 0px 16px;">' + room.text + '</span>')
		} else {
			var $room = $('<span><i class="material-icons" style="font-size:13px; margin:0px 3px 0px 0px; color:#1f4859;">photo_camera</i><span style="position:relative; top:-2px;">' + room.text + '</span></span');
			return $room
		}
	}

	// //sort list options
	// var reA = /[^a-zA-Z]/g;
	// var reN = /[^0-9]/g;

	// _roomdata.sort(function (a, b) {
	// 	var aA = a.replace(reA, "");
	// 	var bA = b.replace(reA, "");
	// 	if(aA === bA) {
	// 		return 1;
	// 	} else if (a.text < b.text) {
	// 		var aN = parseInt(a.replace(reN, ""), 10);
	// 		var bN = parseInt(b.replace(reN, ""), 10);
	// 		return aN === bN ? 0 : aN > bN ? 1 : -1;
	// 	} else {
	// 		return aA > bA ? 1 : -1;
	// 	}
	// });

	//destroy existing select2
	$('#room-menu select').select2('destroy');
	$('#room-menu').html('<select><select><i style="display:none;" class="clear material-icons">close</i>');

	//init room menu
	var roommenu = d3.select("#room-menu select");

	//populate selection list
	roommenu.selectAll('option')
		.data(d3.map(_roomdata, function (d) {return d.room }).keys())
		.enter().append('option')
		.text(function(d) { return d.replace('\'','') });

	//insert empty option tag for default selection
	roommenu._groups[0][0].innerHTML = '<option></option>' + roommenu._groups[0][0].innerHTML;

	//reveal room menu
	d3.select('#room-menu').style('display','inline-block');
	
	//initialize selection list options
	$('#room-menu select').select2({
		placeholder: "Select Room",
		dropdownAutoWidth : true,
		width: '400px',
		formatResult: formatRoom, //add camera img if photos are available
	}).on('change', selectRoom);

	function selectRoom() {

		//update active room variable
		if (roomfiles.filter(function(d) { return d.bldg == active_bldg.bldg && d.room == roommenu.property('value') }).length > 0) {
			active_room = roomfiles.filter(function(d) { return d.bldg == active_bldg.bldg && d.room == roommenu.property('value') })[0];
		} else {
			active_room = {'bldg':active_bldg.bldg,'room':roommenu.property('value'),'fileid':'none'}
		}
		
		//reveal clear button
		d3.select('#room-menu .clear')
			.style('display','inline-block')
			.on('click',function() {
				
				//reset room selection list
				listRooms();

				//reset active room
				active_room = '';

				//clear existing photo holder elements
				d3.select('#photo-frame').html('');
				d3.select('#thumbnail-liner').html('');

				//clear + hide data table
				d3.select('#room-info').style('display','none').html('');

				//hide clear button
				d3.select('#room-menu .clear').style('display','none');
			});

		//init viewers for photos and info
		initPhotos();
		initInfo();
	}
}

function initPhotos() {

	//clear existing photo holder elements
	d3.select('#photo-frame').html('');
	d3.select('#thumbnail-liner').html('');

	if (active_room.fileid == 'none') {
		//display nophoto modal
		d3.select('#nophoto-modal').style('display','block');

	} else {
		//hide nophoto modal
		d3.select('#nophoto-modal').style('display','none');

		//grab photos in active orom file
		gapi.client.drive.files.list({
			'orderBy': "folder, name",
			'q': "(\'" + active_room.fileid + "\' in parents)",
			'fields': "nextPageToken, files(id, name, thumbnailLink, imageMediaMetadata)",
			'pageSize': 1000,
		}).then(function(response) {
			
			var photos = response.result.files;

			//loop photos
			for (var i = 0; i < photos.length; i++) {
				//determin width of thumbnails
				if (photos[i].imageMediaMetadata.rotation == 0 || photos[i].imageMediaMetadata.rotation == 2) {
					var w = (photos[i].imageMediaMetadata.width*114)/photos[i].imageMediaMetadata.height;
				} else {
					var w = (photos[i].imageMediaMetadata.height*114)/photos[i].imageMediaMetadata.width;
				}

				//update thumbnail liner
				d3.select('#thumbnail-liner')
					.html($('#thumbnail-liner').html() + '<div class="thumbnail-holder" id="' + photos[i].id + '" style="width:' + w + 'px;"><img class="thumbnail" name="' + photos[i].id + '" src="' + photos[i].thumbnailLink + '"</img></div>');
			}

			//add thumbnail listeners
			d3.selectAll('.thumbnail').on('click',function() {
				d3.select('#photo-frame')
					.html('<iframe src=\"https://drive.google.com/file/d/' + this.name + '/preview\" style="width:100%; height:100%; border:none;"></iframe>');
			});

			//add first photo to photo frame by default
			d3.select('#photo-frame')
				.html('<iframe src=\"https://drive.google.com/file/d/' + photos[0].id + '/preview\" style="width:100%; height:100%; border:none;"></iframe>');
		});
	}
}

function initInfo() {
	//remove existing tables
	d3.selectAll('#room-info table, #room-info div').remove();

	//reveal room-info element
	d3.select('#room-info').style('display','block');

	//pull room data
	var _data = roomdata.filter(function(d) { return d.link == active_room.bldg + '-' + active_room.room })[0];

	//configure table data
	var aimtable = [
		{'field':'Floor Level','value':_data.room_floor},
		{'field':'Room SqFt','value':_data.room_sf},
		{'field':'Instr. Capacity','value':_data.room_icap},
		{'field':'NCES Use Category','value':'(' + _data.nces_c_code + ') ' + _data.nces_c_descr},
		{'field':'NCES Use Detail','value':'(' + _data.nces_d_code + ') ' + _data.nces_d_descr},
		{'field':'Org. Assignment (1)','value':_data.org_descr1},
		{'field':'Org. Assignment (2)','value':_data.org_descr2},
		{'field':'Org. Assignment (3)','value':_data.org_descr3},
		{'field':'Org. Assignment (4)','value':_data.org_descr4},
		{'field':'Org. Assignment (5)','value':_data.org_descr5},
		{'field':'Org. Assignment (6)','value':_data.org_descr6},
		{'field':'Org. Assignment (7)','value':_data.org_descr7},
		{'field':'Org. Assignment (8)','value':_data.org_descr8},
	]

	var survtable = [
		{'field':'Survery Record','value':_data.access_descr},
		{'field':'Passive Vent. Quality','value':_data.vent_nat},
		{'field':'Active Vent. Quality','value':_data.vent_art},
		{'field':'AC Systems','value':_data.ac_types},
		{'field':'Fan Systems','value':_data.fan_types},
		{'field':'Fenestration','value':_data.fenes},
		{'field':'Natural Light Quality','value':_data.nat_lighting},
		{'field':'Art. Light Quality','value':_data.art_lighting},
		{'field':'Lighting Controls','value':_data.art_light_cont},
		{'field':'Acoustic Quality','value':_data.noise_lvl},
		{'field':'Acoustic Features','value':_data.acous_cont},
		{'field':'Number of Seats','value':_data.seat_num},
		{'field':'Seating Flexibility','value':_data.seat_flex},
		{'field':'Room Features','value':_data.room_feats},
		{'field':'% Used as Storage','value':_data.pct_stor},
		{'field':'% Not in Use','value':_data.pct_unused},
	]

	//create table elements
	d3.select('#room-info').append('div')
		.attr('class','title')
		.style('color','#637e8d')
		.text('AIM DATABASE');
	var aimrows = d3.select('#room-info').append('table')
		.attr('id','aim-table')
		.style('width','calc(100% - 20px)');

	d3.select('#room-info').append('div')
		.attr('class','title')
		.style('color','#637e8d')
		.text('SURVEY ASSESSMENT (SPRING 2017)');
	var survrows = d3.select('#room-info').append('table')
		.attr('id','surv-table')
		.style('width','calc(100% - 20px)');

	//print table rows
	var tr = aimrows.selectAll('tr')
		.data(aimtable).enter()
		.append('tr');
	tr.append('td')
		.html(function(d) { return d.field })
		.style('width','120px');
	tr.append('td')
		.html(function(d) { return d.value })
		.style('width','260px');

	var tr = survrows.selectAll('tr')
		.data(survtable).enter()
		.append('tr');
	tr.append('td')
		.html(function(d) { return d.field })
		.style('width','120px');
	tr.append('td')
		.html(function(d) { return d.value })
		.style('width','260px');
}

function resize() {
    //resize wrapper
    var winHeight = $(window).height();
    $('#content-wrapper').height(winHeight - 115);
}


