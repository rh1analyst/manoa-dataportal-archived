password = sessionStorage.getItem("key0");
username = sessionStorage.getItem("key1");

// Add an additional else if clause to the if statement and add new user's credentials below 
if (username.toLowerCase() == 'user' && password.toLowerCase() == 'mkoffice' 	|| 
		username.toLowerCase() == 'mau' && password.toLowerCase() == 'rainbowwarriors' 	|| 
		username.toLowerCase() == 'jgouveia' && password.toLowerCase() == 'manoa' 	|| 
		username.toLowerCase() == 'rodwell' && password.toLowerCase() == 'rainbowwarriors' ||
		username.toLowerCase() == 'uranakak@hawaii.edu' && password.toLowerCase() == 'rainbowwarriors' ||
		username == 'Guest2019' && password == 'rainbowwarriors') {
    
	console.log('login:success');
} else {
    logout();
}
//commented this out post version addition

function logout() {
    window.open('index.html', '_self')
}
	
//after log in reset to default to ensure some security
username = 'holder'
password = 'holder'
where = 0;

// Function to open Department Visualization
function departmentViz() {
    viz.dispose()
    var placeholderDiv = document.getElementById("content-wrapper");
    // var url = "https://public.tableau.com/views/DepartmentalSpaceAnalysis/Department"
    var url = "https://public.tableau.com/views/DepartmentalSpaceAnalysis_0/Department"
    var options = {
        width: $(window).width() - 18,
        height: $(window).height() - 160 < 950? 950: $(window).height() - 160,
        hideTabs: true,
        hideToolbar: false,
        onFirstInteractive:
            function () {
                workbook = viz.getWorkbook();
                activeSheet = workbook.getActiveSheet();
            }
        };
        
    viz = new tableau.Viz(placeholderDiv, url, options);

    where = 2;
    d3.select("#department").classed('active',true);
    d3.select("#inventory").classed('active',false);
    d3.select("#instruction").classed('active',false);

};

// Function to open Inventory Visualization
function inventoryViz() {

    if (typeof viz != 'undefined') {
        viz.dispose()
    }
    
    var placeholderDiv = document.getElementById("content-wrapper");
    // var url = "https://public.tableau.com/views/FacilitiesInventoryAnalysis/Inventory"
    var url = "https://public.tableau.com/views/FacilitiesInventoryAnalysis_0/Inventory"
    var options = {
        width: $(window).width() - 18,
        height: $(window).height() - 160 < 950? 950: $(window).height() - 160,
        hideTabs: true,
        hideToolbar: false,
        onFirstInteractive:
            function () {
                workbook = viz.getWorkbook();
                activeSheet = workbook.getActiveSheet();
            }
        };
        
    viz = new tableau.Viz(placeholderDiv, url, options);

    where = 1;
    d3.select("#department").classed('active',false);
    d3.select("#inventory").classed('active',true);
    d3.select("#instruction").classed('active',false);
}

// Function to open Instruction Visualization
function instructionViz() {
    viz.dispose()
    var placeholderDiv = document.getElementById("content-wrapper");
    // var url = "https://public.tableau.com/views/InstructionalSpaceAnalysis/Instruction"
    var url = "https://public.tableau.com/views/InstructionalSpaceAnalysis_0/Instruction"
    var options = {
        width: $(window).width() - 18,
        height: $(window).height() - 160 < 950? 950: $(window).height() - 160,
        hideTabs: true,
        hideToolbar: false,
        onFirstInteractive:
            function () {
                workbook = viz.getWorkbook();
                activeSheet = workbook.getActiveSheet();
            }
        };
        
    viz = new tableau.Viz(placeholderDiv, url, options);

    where = 3;
    d3.select("#department").classed('active',false);
    d3.select("#inventory").classed('active',false);
    d3.select("#instruction").classed('active',true);

}

function resize() {
    //resize wrapper
    var winHeight = $(window).height();
    $('#content-wrapper').height(winHeight - 115);
    //reinit viz
    if (where == 2) {
        departmentViz();
    } else if (where == 1){
        inventoryViz();
    } else if (where == 3){
        instructionViz();
    } else {
        inventoryViz();
    }
}
